/* 
 * Name: Nathan Ortiz
 * Student ID: 2531686
 * Date: March 3, 2014
 * HW:1
 * Problem:7
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "**************************************";
    cout << endl;
    cout << "      ccc          ssss         !!    " << endl; 
    cout << "     c   c        s    s        !!    " << endl;
    cout << "    c            s              !!    " << endl;
    cout << "   c              s             !!    " << endl;
    cout << "   c               ssss         !!    " << endl;
    cout << "   c                   s        !!    " << endl;
    cout << "    c                   s       !!    " << endl;
    cout << "     c   c        s    s              " << endl;
    cout << "      ccc          ssss         00    " << endl;
    cout << endl;
    cout << "**************************************" << endl;
    cout << "  Computer Science is Cool Stuff!!!   " << endl;
    return 0;
}

