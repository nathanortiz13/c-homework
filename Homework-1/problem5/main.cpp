/* 
 * Name: Nathan Ortiz
 * Student ID: 2531686
 * Date: March 3, 2015
 * HW: 1
 * Problem:5
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int packages, fitaid, fitaid_total;
    
    cout << "Please enter the number of packages:" << endl;
    cin >> packages;
    cout << "Please enter the number of Fitaid beverages per package: " << endl;
    cin >> fitaid;
    
    fitaid_total = (packages * fitaid);
    
    cout << "If you have ";
    cout << packages;
    cout << " packages" << endl;
    cout << "and have ";
    cout << fitaid;
    cout << " Fitaid beverages per package, then " << endl;
    cout << "you have a total of ";
    cout << fitaid_total;
    cout << " Fitaid beverages." << endl;
    cout << endl;
    cout << "(I know i dont have to put this but i did it anyway because it is good practice)" << endl;
    cout << "this is the end of my program." << endl;
    
    return 0;
}

