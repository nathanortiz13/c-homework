/* 
 * File:   main.cpp
 * Author: natha_000
 *
 * Created on March 10, 2015, 9:15 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    string name, anotherName, food, adjective, color, animal;
    const int maxcharacters = 10;
    int number;
    
    cout << "Hello. We are going to be playing MadLib today! For simplicity, "
            "each input can only be a max of 10 characters. Please enter" << endl;
   
    cout << "A name: ";
    while (1)
    {
        cin >> name;
    if (name.size() > maxcharacters)
    {
        cout << "Input is to long." << endl;
    }
    else
    {
    break;
    }
    }  
    cout << endl;
    
    
    cout << "Another name: ";
    
    while (2)
    {
        cin >> anotherName;
    
    if (anotherName.size() > maxcharacters)
    {
        cout << "Input is to long." << endl;
    }
    else
    {
    break;
    }
    }  
    cout << endl;
    
    cout<< "A food: ";
    while (3)
    {
        cin >> food;
    if (food.size() > maxcharacters)
    {
        cout << "Input is to long." << endl;
    }
    else
    {
    break;
    }
    }  
    cout << endl;
    
    cout << "A number between 100 and 200: ";
    while (4)
    {
        cin >> number;
    if (number > 200)
    {
        cout << "Input is to long." << endl;
    }
    else if (number < 100)
    {
        cout << "Number to small.";
    }
    else
    {
    break;
    }
    }  
    cout << endl;
    
    cout << "An adjective: ";
    while (5)
    {
        cin >> adjective;
    if (adjective.size() > maxcharacters)
    {
        cout << "Input is to long." << endl;
    }
    else
    {
    break;
    }
    }  
    cout << endl;
    
    cout << "A color: ";
    while (6)
    {
        cin >> color;
    if (color.size() > maxcharacters)
    {
        cout << "Input is to long." << endl;
    }
    else
    {
    break;
    }
    }  
    cout << endl;
    
    cout << "An animal: ";
    while (7)
    {
        cin >> animal;
    if (animal.size() > maxcharacters)
    {
        cout << "Input is to long." << endl;
    }
    else
    {
    break;
    }
    }  
    cout << endl;
    
 
    cout << "Dear " << name << ",";
    cout << endl;
    cout << "I am sorry that i am unable to turn in my homework at this time.";
    cout <<" First, i ate a rotten ";
    cout << food;
    cout << ", which made me turn ";
    cout << color;
    cout << endl;
    cout << "and extremely ill.";
    cout << "I came down with a fever of ";
    cout << number << ".";
    cout << "Next, my ";
    cout << adjective;
    cout << " pet ";
    cout << animal;
    cout << " must have smell the remains of the";
    cout << food;
    cout << " on my homework because he ate it.";
    cout << "I am currently rewriting my homework and hope you will accept it late.";
    cout << endl;
    cout << endl;
    cout << "sincerely,";
    cout << endl;
    cout << setw(20) << right << anotherName;
    
    
    return 0;
}

