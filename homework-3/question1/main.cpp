/* 
 * File:   main.cpp
 * Author: natha_000
 *
 * Created on March 15, 2015, 1:15 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    string telephone;
    cout << "Enter a telephone number that requires only one dash in between: ";
    cin >> telephone;
    int locDash = telephone.find("-");
    string firstHalf;
    firstHalf = telephone.substr(0, locDash);
    string secondHalf;
    secondHalf = telephone.substr(locDash + 1);
    cout << firstHalf << secondHalf;
    

    return 0;
}

