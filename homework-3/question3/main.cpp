/* 
 * File:   main.cpp
 * Author: natha_000
 *
 * Created on March 19, 2015, 4:22 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int attending, maxcapacity, addcapacity, subcapacity;
    cout << "Do to fire regulation laws Enter how many people are attending the"
            " meeting: ";
    cin >> attending;
    cout << "Enter the maximum room capacity: ";
    cin >> maxcapacity;

    if ( attending <= maxcapacity)
    {
        addcapacity = maxcapacity - attending;
        cout << "It is legal to hold the meeting." << addcapacity << " people can"
                " be add without exceeding the legal capacity limit.";
    }
    
    else if (attending > maxcapacity)
    {
        subcapacity = attending - maxcapacity;
        cout << "Meeting cannot be held as planned due to fire regulations.";
        cout << subcapacity << " attendees must be excluded in order to meet "
                "fire regulations.";
    }
    
    else 
    {
        cout << "ERROR. invalid input.";
    }
    return 0;
}

