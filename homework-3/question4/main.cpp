/* 
 * File:   main.cpp
 * Author: natha_000
 *
 * Created on March 18, 2015, 9:44 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int positiveTotal = 0;
    for (int i = 0; i < 10; i++)
    {
        int num;
        cout << "Enter numbers: ";
        cin >> num;
        if (num > 0)
        {
            positiveTotal += num;
        }
    }

    return 0;
}

