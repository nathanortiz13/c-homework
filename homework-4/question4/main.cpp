/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 25, 2015, 4:20 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "A typical chocolate bar will contain around 230 calories."
         << "Enter the following information to know how many chocolate bars "
         << "are needed to be consumed in order to maintain one's weight for "
         << "the appropriate sex of the specific weight, height, and age."
         << endl;
    
    int weight, inches, height, age, gender;
    cout << "Enter your weight in pounds: ";
    cin >> weight;
    cout << "Enter your height in inches: ";
    cin >> height;
    cout << "Enter your age: ";
    cin >> age;
    cout << "Enter M for male or F for Female: ";
    cin >> gender;
    
    int female_bmr, male_bmr;
    
    female_bmr = 655 + (4.3 * weight) + (4.7 * inches) - (4.7 * age);
    male_bmr = 66 + (6.3 * weight) + (12.7 * inches) - (6.8 * age);
    
    string response;
    
    while (response == "yes")
    {
    if (gender = "M" || "m")
    {
        cout << male_bmr / 230 << " chocolate bars are needed to be consumed.";
    }
    else if (gender = "F" || "f")
    {
        cout << female_bmr / 230 << " chocolate bars are needed to be consumed."
                ;
    }
    else
    {
        cout << "ERROR. Input not valid";
    }
    cout << "Would you like to calulate again? yes or no: ";
    cin >> response;
    }
    return 0;
}

